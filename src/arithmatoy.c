#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
        fprintf(stderr, "add: entering function\n");
    }

    // Fill the function, the goal is to compute lhs + rhs
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result

    // Calculer la longueur des chaînes pour déterminer la taille du résultat
    int lhs_length = strlen(lhs);
    int rhs_length = strlen(rhs);
    int max_length = lhs_length > rhs_length ? lhs_length : rhs_length;

    // Allocation de mémoire pour stocker le résultat
    char *result = (char *)malloc(max_length + 2);  // +1 pour la retenue et +1 pour le caractère nul
    result[max_length + 1] = '\0'; // Caractère nul pour terminer la chaîne

    int carry = 0; // Initialisation de la retenue
    for (int i = 0; i <= max_length; i++) {
        int digit_lhs = i < lhs_length ? get_digit_value(lhs[lhs_length - 1 - i]) : 0;
        int digit_rhs = i < rhs_length ? get_digit_value(rhs[rhs_length - 1 - i]) : 0;
        int sum = digit_lhs + digit_rhs + carry;
        result[max_length - i] = to_digit(sum % base);
        carry = sum / base;

        if (VERBOSE) {
            fprintf(stderr, "Step %d: %d + %d + %d (carry) = %d, result digit = %c\n", i, digit_lhs, digit_rhs, carry, sum, result[max_length - i]);
        }
    }

    // Suppression des zéros non significatifs
    char *final_result = strdup(drop_leading_zeros(result));
    free(result);
    return final_result;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
        fprintf(stderr, "sub: entering function\n");
    }

    // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result

    int lhs_length = strlen(lhs);
    int rhs_length = strlen(rhs);
    int max_length = lhs_length > rhs_length ? lhs_length : rhs_length;

    // Gérer le cas où lhs est inférieur à rhs, retourne NULL dans ce cas
    if (strcmp(lhs, rhs) < 0 && lhs_length <= rhs_length) {
        if (VERBOSE) {
            fprintf(stderr, "sub: lhs is less than rhs, returning NULL\n");
        }
        return NULL;
    }

    char *result = (char *)malloc(max_length + 1); // Plus one for null-terminator
    result[max_length] = '\0';

    int borrow = 0; // Initialisation de l'emprunt
    for (int i = 0; i < max_length; i++) {
        int digit_lhs = i < lhs_length ? get_digit_value(lhs[lhs_length - 1 - i]) : 0;
        int digit_rhs = i < rhs_length ? get_digit_value(rhs[rhs_length - 1 - i]) : 0;
        int diff = digit_lhs - digit_rhs - borrow;
        if (diff < 0) {
            diff += base;
            borrow = 1;
        } else {
            borrow = 0;
        }
        result[max_length - 1 - i] = to_digit(diff);

        if (VERBOSE) {
            fprintf(stderr, "Step %d: %d - %d - %d (borrow) = %d, result digit = %c\n", i, digit_lhs, digit_rhs, borrow, diff, result[max_length - 1 - i]);
        }
    }

    // Suppression des zéros non significatifs et vérification si le résultat est vide
    const char *trimmed_result = drop_leading_zeros(result);
    char *final_result = trimmed_result[0] == '\0' ? strdup("0") : strdup(trimmed_result);
    free(result);
    return final_result;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
        fprintf(stderr, "mul: entering function\n");
    }

    // Fill the function, the goal is to compute lhs * rhs
    // You should allocate a new char* large enough to store the result as a
    // string Implement the algorithm Return the result

    int lhs_length = strlen(lhs);
    int rhs_length = strlen(rhs);
    int result_length = lhs_length + rhs_length;  // Maximum possible length for the result

    // Initialisation du tableau de résultats avec des zéros
    int *temp_result = calloc(result_length, sizeof(int));
    if (!temp_result) {
        perror("Failed to allocate memory for multiplication result");
        exit(EXIT_FAILURE);
    }

    // Multiplication de chaque chiffre
    for (int i = 0; i < lhs_length; i++) {
        int lhs_digit = get_digit_value(lhs[lhs_length - 1 - i]);
        for (int j = 0; j < rhs_length; j++) {
            int rhs_digit = get_digit_value(rhs[rhs_length - 1 - j]);
            int index = i + j;
            int product = lhs_digit * rhs_digit + temp_result[index];
            temp_result[index] = product % base;
            temp_result[index + 1] += product / base;

            if (VERBOSE) {
                fprintf(stderr, "Multiplying: %d * %d, add %d to [%d], carry %d to [%d]\n",
                        lhs_digit, rhs_digit, temp_result[index] - (product / base) * base,
                        index, product / base, index + 1);
            }
        }
    }

    // Convertir le tableau de résultats en chaîne de caractères et gérer les zéros de tête
    char *result = malloc(result_length + 1);
    result[result_length] = '\0';
    for (int k = 0; k < result_length; k++) {
        result[k] = to_digit(temp_result[result_length - 1 - k]);
    }

    char *final_result = strdup(drop_leading_zeros(result));
    if (final_result[0] == '\0') {  // Gérer le cas où le résultat est tout zéro
        strcpy(final_result, "0");
    }
    free(result);
    free(temp_result);
    return final_result;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
